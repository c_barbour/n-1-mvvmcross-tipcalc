using Cirrious.MvvmCross.ViewModels;
using TipCalc.Core.Services;

namespace TipCalc.Core.ViewModels
{
    public class FirstViewModel 
		: MvxViewModel
    {

		private readonly ICalculationService _calculationService;

		public FirstViewModel(ICalculationService calcuationService)
		{
			_calculationService = calcuationService;
			_generosity = 20;
			_subTotal = 100;
			RecalcuateTip();
		}

		private double _total;
		public double Total
		{
			get { return _total; }
			set 
			{ 
				_total = System.Math.Round(value * 100) / 100.0f; 
				RaisePropertyChanged(() => Total);
			}
		}

		private double _subTotal;
		public double SubTotal
		{
			get { return _subTotal; }
			set 
			{ 
				_subTotal = System.Math.Round(value * 100) / 100.0f; 
				RaisePropertyChanged(() => SubTotal); 
				RecalcuateTip();
			}
		}

		private double _tip;
		public double Tip
		{
			get { return _tip; }
			set 
			{ 
				_tip = System.Math.Round(value * 100) / 100.0f; 
				RaisePropertyChanged(() => Tip); 
			}
		}

		private double _generosity;
		public double Generosity
		{
			get { return _generosity; }
			set 
			{ 
				_generosity = value; 
				RaisePropertyChanged(() => Generosity); 
				RecalcuateTip();
			}
		}

		private void RecalcuateTip()
		{
			Tip = _calculationService.Tip(SubTotal, Generosity);
			Total = SubTotal + Tip;
		}
    }
}
