﻿using System;

namespace TipCalc.Core.Services
{
	public interface ICalculationService
	{
		double Tip (double subtotal, double generosity);
	}
}

