﻿using System;

namespace TipCalc.Core.Services
{
	public class CalculationService: ICalculationService
	{
		public CalculationService()
		{
		}

		#region ICalculationService implementation

		public double Tip(double subtotal, double generosity)
		{
			return subtotal*generosity/100.0f;
		}

		#endregion
	}
}

