using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Touch.Views;
using CoreGraphics;
using Foundation;
using ObjCRuntime;
using UIKit;
using System.Drawing;

namespace TipCalc.iOS.Views
{
    [Register("FirstView")]
    public class FirstView : MvxViewController
    {
        public override void ViewDidLoad()
        {
            View = new UIView { BackgroundColor = UIColor.White };
            base.ViewDidLoad();

			// ios7 layout
            if (RespondsToSelector(new Selector("edgesForExtendedLayout")))
            {
               EdgesForExtendedLayout = UIRectEdge.None;
            }
			    
			var labelSubtotal = new UILabel(new RectangleF(10, 10, 300, 40));
			labelSubtotal.Text = "Subtotal:";
			Add(labelSubtotal);

			var textSubtotal = new UITextField(new RectangleF(10, 30, 300, 40));
			Add(textSubtotal);

			var labelGenerosity = new UILabel(new RectangleF(10, 50, 300, 40));
			labelGenerosity.Text = "Generosity:";
			Add(labelGenerosity);

			var progressGenerosity = new UISlider(new RectangleF(10, 70, 300, 40));
			progressGenerosity.MinValue = 0;
			progressGenerosity.MaxValue = 100;
			Add(progressGenerosity);

			var labelTip = new UILabel(new RectangleF(10, 90, 300, 40));
			labelTip.Text = "Tip:";
			Add(labelTip);

			var labelTipText = new UILabel(new RectangleF(10, 110, 300, 40));
			Add(labelTipText);

			var labelTotal = new UILabel(new RectangleF(10, 130, 300, 40));
			labelTotal.Text = "Total:";
			Add(labelTotal);

			var labelTotalText = new UILabel(new RectangleF(10, 150, 300, 40));
			Add(labelTotalText);

            var set = this.CreateBindingSet<FirstView, TipCalc.Core.ViewModels.FirstViewModel>();
			set.Bind(textSubtotal).To(vm => vm.SubTotal);
			set.Bind(progressGenerosity).To(vm => vm.Generosity);
			set.Bind(labelTipText).To(vm => vm.Tip);
			set.Bind(labelTotalText).To(vm => vm.Total);
            set.Apply();
        }
    }
}